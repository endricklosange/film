<?php
require_once 'db/connect.php';
require_once 'component/head.php';
require_once 'component/navbar.php';
$pdo = new \PDO(DSN, USER, PASS);$urlID = str_replace('/', '', $_SERVER['PATH_INFO']);
session_start();
$query = "SELECT * FROM `film`INNER JOIN categories ON category = categories.id WHERE film.id = $urlID;";
$statement = $pdo->query($query);
$movie = $statement->fetch();
?>
<html lang="fr">

<head>
    <?php
    head()    ?>

    <title><?= $movie['title'] ?></title>
</head>

<body>
    <?php
    navbar()    ?>
    <div class="container">
        <h1><?= $movie['title'] ?></h1>
        <div><?= $movie['video'] ?></div>
        <div class="row">
            <div class="col-6 mt-5">
                <img src="<?= $movie['image'] ?>" alt="image d'affiche de<?= $movie['title'] ?>">
            </div>
            <div class="col-6">
                <p> Durée: <?= $movie['durée'] ?></p>
                <p> Acteurs: <?= $movie['actor'] ?></p>
                <p> Durée: <?= $movie['durée'] ?></p>
                <p> Classification parental: <?= $movie['parentalClassification'] ?></p>
                <p> Genre: <?= $movie['name'] ?></p>
            </div>
        </div>
        <div>
            <p><?= $movie['description'] ?></p>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>

</html>