<?php
require_once 'db/connect.php';
require_once 'class\Users.php';
require_once 'component/head.php';
require_once 'component/navbar.php';
require_once 'class/Registration.php';
$pdo = new \PDO(DSN, USER, PASS);
session_start();

if (isset($_POST['submit'])) {
  $userClass = new Users($_POST['user_email'], $_POST['user_password']);
  $signIn = new Registration($userClass);
} 

?>
<!DOCTYPE html>
<html lang="fr">

<head>
  <?php
  head()
  ?>

  <title>Document</title>
</head>

<body>
  <?php
  navbar()
  ?>
  <div class="container">
  <h1 class="my-3">Inscription</h1>
    <form action="formRegistration.php"class="formRegistration" method="post">
      <div class="form-group">
        <label for="exampleInputEmail1">Email</label>
        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" name="user_email">
      </div>
      <div class="form-group mb-2">
        <label for="exampleInputPassword1">Mot de passe</label>
        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="user_password">
      </div>
      <button type="submit" name="submit" class="btn btn-primary">Go</button>
      <a class="btn btn-primary my-3" href="/film/formLogin.php">Connexion</a>
    </form>


  </div>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>

</html>