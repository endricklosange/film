<?php
require_once 'db/connect.php';
require_once 'component/head.php';
require_once 'component/navbar.php';
require_once 'class/Users.php';
session_start();

$pdo = new \PDO(DSN, USER, PASS);
$CurrentUserID = $_SESSION['Log']->getId();
$query = "SELECT * FROM `favorites` JOIN film ON film_id JOIN `user` ON user_id WHERE user.id =`user_id` && film.id = `film_id`;
";
$statement = $pdo->query($query);
$movies = $statement->fetchAll();
$userDbId = $movies[0]['user_id'];

?>
<html lang="fr">

<head>
    <?php
    head()
    ?>
    <title>Document</title>
</head>

<body>
    <?php
    navbar()
    ?>
    <div class="container">
        <h1>Favoris</h1>
        <div class="row">
            <?php
            foreach ($movies as $movie) { ?>
                <?php if ($movie['user_id'] == $CurrentUserID) { ?>
                    <div class="card col-6">
                        <img class="card-img-top" src="<?= $movie['image'] ?>" alt="Card image cap" style="width:190px;" />
                        <div class="card-body">
                            <h2 class="card-title"><?= $movie['title'] ?></h2>
                            <p class="card-text"><?= substr($movie['description'], 0, 100) . '...'; ?></p>
                            <a href="/film/show.php/<?= $movie[0] ?>" class="btn btn-primary">Voir</a>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>

</html>