-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : ven. 13 mai 2022 à 01:56
-- Version du serveur : 10.4.20-MariaDB
-- Version de PHP : 7.4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `motion_streaming`
--
CREATE DATABASE IF NOT EXISTS `motion_streaming` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `motion_streaming`;

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `categories`
--

INSERT INTO `categories` (`id`, `name`) VALUES
(1, 'Horreur'),
(2, 'Aventure'),
(3, 'Actions'),
(6, 'Drame');

-- --------------------------------------------------------

--
-- Structure de la table `favorites`
--

CREATE TABLE `favorites` (
  `film_id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `favorites`
--

INSERT INTO `favorites` (`film_id`, `user_id`) VALUES
(3, 13),
(8, 13),
(3, 13),
(8, 17),
(9, 17),
(9, 17),
(3, 17),
(1, 17),
(7, 17),
(4, 17);

-- --------------------------------------------------------

--
-- Structure de la table `film`
--

CREATE TABLE `film` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `durée` varchar(255) NOT NULL,
  `actor` varchar(255) NOT NULL,
  `parentalClassification` int(255) NOT NULL,
  `video` varchar(1000) NOT NULL,
  `category` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `film`
--

INSERT INTO `film` (`id`, `title`, `image`, `description`, `durée`, `actor`, `parentalClassification`, `video`, `category`) VALUES
(1, 'FREAKY', 'https://fr.web.img4.acsta.net/c_310_420/pictures/20/10/19/17/15/3660884.jpg', 'Pas très sûre d\'elle, Millie Kessler, 17 ans, tente de se faire remarquer le moins possible au lycée et de s\'endurcir face aux sarcasmes des élèves les plus populaires. Jusqu\'au jour où elle croise la route du Boucher, terrifiant tueur en série de la ville : sous l\'effet du mystérieux poignard de l\'assassin, Millie et son assaillant se retrouvent chacun dans le corps de l\'autre ! La jeune fille ne dispose désormais que de 24 heures pour récupérer son corps – passé ce délai, elle restera coincée dans la peau d\'un psychopathe de 50 ans !\r\nAvec ses fidèles amis Nyla et Joshua, elle se lance dans une course contre la montre pour inverser le sort. Sauf qu\'elle a le physique d\'un tueur activement recherché par la police, tandis que le Boucher, lui, s\'aperçoit que ressembler à une adolescente au visage angélique est une couverture idéale pour commettre son carnage…', '1h 42min', 'Vince Vaughn, Kathryn Newton, Celeste O’Connor', 18, '<iframe src=\'https://player.allocine.fr/19589834.html\' style=\'width:480px; height:270px\'></iframe>', 1),
(3, 'TEDDY', 'https://fr.web.img6.acsta.net/c_310_420/pictures/21/07/05/13/09/5598862.jpg', 'Dans les Pyrénées, un loup attise la colère des villageois. Teddy, 19 ans, sans diplôme, vit avec son oncle adoptif et travaille dans un salon de massage. Sa petite amie Rebecca passe bientôt son bac, promise à un avenir radieux. Pour eux, c’est un été ordinaire qui s’annonce. Mais un soir de pleine lune, Teddy est griffé par une bête inconnue. Les semaines qui suivent, il est pris de curieuses pulsions animales…', '1h 28min', 'Anthony Bajon, Christine Gautier, Ludovic Torrent', 18, '<iframe src=\'https://player.allocine.fr/19589491.html\' style=\'width:480px; height:270px\'></iframe>\r\n', 1),
(4, 'LES ANIMAUX FANTASTIQUES : LES SECRETS DE DUMBLEDORE', 'https://fr.web.img6.acsta.net/c_310_420/pictures/22/03/16/15/20/0170262.jpg', 'Le professeur Albus Dumbledore sait que le puissant mage noir Gellert Grindelwald cherche à prendre le contrôle du monde des sorciers. Incapable de l’empêcher d’agir seul, il sollicite le magizoologiste Norbert Dragonneau pour qu’il réunisse des sorciers, des sorcières et un boulanger moldu au sein d’une équipe intrépide. Leur mission des plus périlleuses les amènera à affronter des animaux, anciens et nouveaux, et les disciples de plus en plus nombreux de Grindelwald. Pourtant, dès lors que que les enjeux sont aussi élevés, Dumbledore pourra-t-il encore rester longtemps dans l’ombre ?', '2h 22min', 'Eddie Redmayne, Jude Law, Mads Mikkelsen', 10, '<iframe src=\'https://player.allocine.fr/19595827.html\' style=\'width:480px; height:270px\'></iframe>', 2),
(5, 'DOCTOR STRANGE IN THE MULTIVERSE OF MADNESS\r\n', 'https://fr.web.img3.acsta.net/c_310_420/pictures/22/04/08/10/30/1779137.jpg', 'Dans ce nouveau film Marvel Studios, l’univers cinématographique Marvel déverrouille et repousse les limites du multivers encore plus loin. Voyagez dans l’inconnu avec Doctor Strange, qui avec l’aide d’anciens et de nouveaux alliés mystiques, traverse les réalités hallucinantes et dangereuses du multivers pour affronter un nouvel adversaire mystérieux.', '2h 06min', 'Benedict Cumberbatch, Elizabeth Olsen, Chiwetel Ejiofor', 10, '<iframe src=\'https://player.allocine.fr/19595154.html\' style=\'width:480px; height:270px\'></iframe>\r\n\r\n', 2),
(6, 'LE SECRET DE LA CITÉ PERDUE', 'https://fr.web.img6.acsta.net/c_310_420/pictures/22/02/22/12/00/1445602.jpg', 'Loretta Sage, romancière brillante mais solitaire, est connue pour ses livres mêlant romance et aventures dans des décors exotiques. Alan, mannequin, a pour sa part passé la plus grande partie de sa carrière à incarner Dash, le héros à la plastique avantageuse figurant sur les couvertures des livres de Loretta. Alors qu’elle est en pleine promotion de son nouveau roman en compagnie d’Alan, Loretta se retrouve kidnappée par un milliardaire excentrique qui est persuadé qu’elle pourra l’aider à retrouver le trésor d’une cité perdue évoquée dans son dernier ouvrage. Déterminé à prouver qu’il peut être dans la vraie vie à la hauteur du héros qu’il incarne dans les livres, Alan se lance à la rescousse de la romancière. Propulsés dans une grande aventure au cœur d’une jungle hostile, ce duo improbable va devoir faire équipe pour survivre et tenter de mettre la main sur l’ancien trésor avant qu’il ne disparaisse à jamais.\r\n', '1h 52min ', 'Sandra Bullock, Channing Tatum, Daniel Radcliffe', 10, '<iframe src=\'https://player.allocine.fr/19595094.html\' style=\'width:480px; height:270px\'></iframe>\r\n', 3),
(7, 'UN TALENT EN OR MASSIF', 'https://fr.web.img3.acsta.net/c_310_420/pictures/22/03/24/17/55/5001114.jpg', 'Nicolas Cage est maintenant un acteur endetté qui attend le grand rôle qui relancera sa carrière. Pour rembourser une partie de ses dettes, son agent lui propose de se rendre à l’anniversaire d’un dangereux milliardaire qui se révèle être son plus grand fan. Mais le séjour prend une toute autre tournure, lorsque la CIA le contacte, lui demandant d’enquêter sur les activités criminelles de son hôte. Nicolas Cage va devoir jouer le rôle de sa vie et prouver qu’il est à la hauteur de sa propre légende.\r\n', '1h 48min', 'Nicolas Cage, Pedro Pascal, Tiffany Haddish', 10, '<iframe src=\'https://player.allocine.fr/19595062.html\' style=\'width:480px; height:270px\'></iframe>\r\n', 3),
(8, 'Strangers from Hell', 'https://www.nautiljon.com/images/drama/00/72/mini/strangers_from_hell_3927.jpg?11649957342', 'Après avoir déniché un stage dans une petite entreprise de Séoul, Yun Jong Wu emménage dans une résidence à prix réduit. D\'ailleurs, l\'endroit est insalubre et sa chambre ressemble à un placard. Il n\'est pourtant pas au bout de ses surprises lorsqu\'ils découvrent que les résidents du lieu ont un comportement étrange ainsi que suspicieux.\r\n\r\nOn dit que « L\'enfer c\'est les autres ». Serait-ce possible que Jong Wu vienne de pénétrer aux marches de l\'enfer ? Un enfer spécialement conçu pour faire surgir les démons intérieurs.\r\n\r\nJong Wu réussira-t-il à sortir saint d\'esprit de toute cette mésaventure ?', '10 x 60 min', 'Yim Si Wan, Ahn Eun Jin, Lee Dong Wook, Kim Ji Eun', 15, '<iframe width=\"600\" height=\"450\" src=\"https://www.youtube.com/embed/htSzSp0KMQY\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', 6),
(9, 'Itaewon Class', 'https://www.nautiljon.com/images/drama/00/75/mini/itaewon_class_4157.jpg?11643370247', 'Lors de son premier jour dans un nouveau lycée, Park Sae Roy frappe l\'un de ses camarades, Jang Geun Won, pour défendre un élève qui se faisait harceler. La brute s\'avère être le fils du PDG Jang Dae Hee, qui n\'est autre que le dirigeant de l\'entreprise de restauration Jangga, où travaille le père de Park Sae Roy. Jang Dae Hee exige de Park Sae Roy qu\'il présente des excuses à son fils, mais il refuse. Ce refus va avoir pour conséquences son exclusion du lycée et la perte du travail de son père.\r\n\r\nPeu de temps après, le père de Park Sae Roy meurt dans un accident de moto causé par son ex-camarade Jang Geun Won. Dans un excès de colère Park Sae Roy bat violemment Jang Geun Won. Suite à cela, il est arrêté et condamné à une peine de prison pour agression violente.\r\nPark Sae Roy décide alors de détruire la compagnie Jangga et de prendre sa revanche sur le PDG Jang Dae Hee et son fils Jang Geun Won.\r\n\r\nUne fois sorti de prison, il ouvre un restaurant dans Itaewon, un quartier de Séoul.\r\n\r\nJo Yi Seo, populaire sur les réseaux sociaux, rejoint le restaurant de Park Sae Roy en tant que manager, et ne tarde pas à développer des sentiments pour ce dernier.', '16 x 70 min', 'Park Seo Jun, Kim Da Mi, Yoo Jae Myung', 15, '<iframe width=\"600\" height=\"450\" src=\"https://www.youtube.com/embed/uxJzM7ssZJU\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', 6);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) UNSIGNED NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `adress` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `password`, `email`, `lastname`, `firstname`, `adress`, `city`, `postal_code`) VALUES
(13, 'test', 'endricklosange@gmail.fr', 'losange', 'endrick', '10rue', 'ascoux', '45300'),
(14, '$2y$10$.N5d/YFuguHxA0g8dKVLvO5g1macVkLunmGSShd1PbmEhwgeRISzO', 'end@test.fr', NULL, NULL, NULL, NULL, NULL),
(16, '$2y$10$gTOmns16LopqUCnbb3u9bOuc/oZqdmB/jk4UIAgxc4bMy2who8E66', 'endrick@gmail.com', NULL, NULL, NULL, NULL, NULL),
(17, '$2y$10$6sN.IxDjdQC7suhTS0NIVOuLX2SZFx5Ro8SxScshwbRrqesMUYZfu', 'endricklosange@gmail.com', 'Losange', 'Endrick', '10 RUE DU PERE BERTHEAU', 'ASCOUX', '45300');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `favorites`
--
ALTER TABLE `favorites`
  ADD KEY `favorites_ibfk_1` (`film_id`),
  ADD KEY `favorites_ibfk_2` (`user_id`);

--
-- Index pour la table `film`
--
ALTER TABLE `film`
  ADD PRIMARY KEY (`id`),
  ADD KEY `film_actor` (`actor`),
  ADD KEY `category_film` (`category`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `film`
--
ALTER TABLE `film`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `favorites`
--
ALTER TABLE `favorites`
  ADD CONSTRAINT `favorites_ibfk_1` FOREIGN KEY (`film_id`) REFERENCES `film` (`id`),
  ADD CONSTRAINT `favorites_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `film`
--
ALTER TABLE `film`
  ADD CONSTRAINT `category_film` FOREIGN KEY (`category`) REFERENCES `categories` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
