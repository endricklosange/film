<?php

function navbar()
{
?>
  <nav class="navbar navbar-expand-lg navbar-light navTop">
    <div class="logo me-5">
      <a href="/film/">
        <img class="header-main-logo-img" src="https://assets.allocine.fr/skin/img/allocine/logo-main-ab1b33daf0.svg" width="162" height="37" alt="allocine">
      </a>
    </div>
    <form class="d-flex" action="/film/search.php" method="POST">
      <input class="form-control me-2" type="search" placeholder="Rechercher un film" aria-label="Search" name="search">
      <button class="btn btn-outline-success submitGlass" name="submitSearch" type="submit"><i class="fa-regular fa-magnifying-glass"></i></button>
    </form>
  </nav>
  <nav class="navbar navbar-expand-lg navbar-light navBottom">
    <a class="navbar-brand" href="#"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item active">
          <a class="nav-link" href="/film">Acceuil <span class="sr-only"></span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/film/categories.php">Categorie</a>
        </li>

        <?php if (!isset($_SESSION["Log"])) { ?>
          <li class="nav-item">
            <a class="nav-link" href="/film/formLogin.php">Connexion</a>
          </li>
        <?php } ?>
        <?php if (isset($_SESSION["Log"])) { ?>
          <li class="nav-item">
            <a class="nav-link" href="/film/favorites.php">Favoris</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/film/formEdit.php">Editer</a>
          </li>
        <?php } ?>

      </ul>
    </div>
  </nav>

<?php
}
