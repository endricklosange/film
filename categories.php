<?php
require_once 'db/connect.php';
require_once 'component/head.php';
require_once 'component/navbar.php';

$pdo = new \PDO(DSN, USER, PASS);
session_start();

$query = "SELECT * FROM `categories`";
$statement = $pdo->query($query);
$categories = $statement->fetchAll();
if (isset($_SESSION["Log"])) {
?>
    <html lang="fr">

    <head>
        <?php
        head()
        ?>

        <title>Document</title>
    </head>

    <body>
        <?php
        navbar()
        ?>
        <div class="container">
            <h1>Categorie</h1>
            <div class="row">
                <?php
                foreach ($categories as $category) { ?>
                    <div class="jumbotron my-3  col-3">
                        <a class="btn btn-primary btn-lg" href="/film/showFilmCategorie.php/<?= $category['id'] ?>" role="button">
                            <h2 class="display-4"><?= $category['name'] ?></h2>
                        </a>
                    </div>
                <?php } ?>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    </body>

    </html>
<?php
} else {
    header("Location: ./formLogin.php");
}
