<?php

require_once 'db/connect.php';
require_once 'class\Users.php';
require_once 'component/head.php';
require_once 'component/navbar.php';
require_once 'class/Edit.php';
session_start();
if(isset($_SESSION['Log'])){
  $CurrentUserID = $_SESSION['Log']->getId();
}
$pdo = new \PDO(DSN, USER, PASS);

if (isset($_POST['submitEdit'])) {
  $userClass = new Users($_POST['user_email'], $_POST['user_password']);
  $userClass->setId($CurrentUserID);
  $userClass->setAddress($_POST['user_adress']);
  $userClass->setCity($_POST['user_city']);
  $userClass->setFirstName($_POST['user_firstname']);
  $userClass->setLastName($_POST['user_lastname']);
  $userClass->setPostalCode($_POST['user_postalCode']);
  $signIn = new Edit($userClass);
} 

?>
<!DOCTYPE html>
<html lang="fr">

<head>
  <?php
  head()
  ?>

  <title>Document</title>
</head>

<body>
  <?php
  navbar()
  ?>
  <div class="container">
    <form method="post">
      <div class="form-group">
        <label for="exampleInputEmail1">Email</label>
        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" name="user_email">
      </div>
      <div class="form-group mb-2">
        <label for="exampleInputPassword1">Mot de passe</label>
        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="user_password">
      </div>
      <div class="form-group mb-2">
        <label for="exampleInputPassword1">nom</label>
        <input type="text" class="form-control"  placeholder="nom" name="user_firstname">
      </div>
      <div class="form-group mb-2">
        <label for="exampleInputPassword1">Prénom</label>
        <input type="text" class="form-control" placeholder="Prénom" name="user_lastname">
      </div>
      <div class="form-group mb-2">
        <label for="exampleInputPassword1">adresse</label>
        <input type="text" class="form-control" placeholder="adresse" name="user_adress">
      </div>
      <div class="form-group mb-2">
        <label for="exampleInputPassword1">Ville</label>
        <input type="text" class="form-control" placeholder="ville" name="user_city">
      </div>
      <div class="form-group mb-2">
        <label for="exampleInputPassword1">Code Postal</label>
        <input type="text" class="form-control" placeholder="Code Postal" name="user_postalCode">
      </div>
      <button type="submit" name="submitEdit" class="btn btn-primary">Modifier</button>
    </form>


  </div>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>

</html>