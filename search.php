<?php
require_once 'db/connect.php';
require_once 'component/head.php';
require_once 'component/navbar.php';
$pdo = new \PDO(DSN, USER, PASS);

if(isset($_POST['submitSearch'])){

    $searchStr = $_POST['search'];
    $query = "SELECT film.id,`title`,`image`,`description`,`image` FROM `film` WHERE `title`LIKE '%$searchStr%';";
    $statement = $pdo->query($query);
    $moviesSearch = $statement->fetchAll(); 
    ?>
    <html lang="fr">
    
    <head>
        <?php
        head()
        ?>
    
        <title>Document</title>
    </head>
    
    <body>
        <?php
        navbar()
        ?>
        <div class="container">
            <div class="row">
                <?php
                foreach ($moviesSearch as $movie) { ?>
                    <div class="card col-4">
                        <img class="card-img-top" src="<?= $movie['image'] ?>" alt="Card image cap" style="width:190px;" />
                        <div class="card-body">
                            <h2 class="card-title"><?= $movie['title']
                             ?></h2>
                            <p class="card-text"><?= substr($movie['description'], 0, 100) . '...'; ?></p>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    </body>
    
    </html>
<?php } ?>
