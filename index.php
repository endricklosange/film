<?php
require_once 'db/connect.php';
require_once 'component/navbar.php';
require_once 'component/head.php';
require_once 'class/Users.php';
session_start();

$pdo = new \PDO(DSN, USER, PASS);
if ($_SERVER['REQUEST_URI'] === '/film/') {
  $query = "SELECT film.id,`title`, categories.id,`image`,`description`,`image`,`name` FROM `film` INNER JOIN categories ON category = categories.id ORDER BY film.id DESC LIMIT 5";
  $statement = $pdo->query($query);
  $movies = $statement->fetchAll();
  /**********************************/
  $queryFav = "SELECT * FROM `favorites` JOIN film ON film_id JOIN `user` ON user_id WHERE user.id =`user_id` && film.id = `film_id` ORDER BY film.id DESC LIMIT 5;";
  $statementFav = $pdo->query($queryFav);
  $moviesFav = $statementFav->fetchAll();
  $userDbId = $moviesFav[0]['user_id'];
  if(isset($_SESSION['Log'])){
    $CurrentUserID = $_SESSION['Log']->getId();
  }else{
    $CurrentUserID = false;
  }
?>
  <html lang="fr">

  <head>
    <?php
    head()
    ?>
    <title>Document</title>
  </head>

  <body>
    <?php
    navbar()
    ?>
    <div class="container">
      <div class="row">
        <?php
        if (!$userDbId == $CurrentUserID) {
          foreach ($movies as $movie){
        ?>
            <div class="card col-3">
              <img class="card-img-top" src="<?= $movie['image'] ?>" alt="Card image cap" style="width:190px;" />
              <div class="card-body">
                <h2 class="card-title"><?= $movie['title'] ?></h2>
                <p><?= $movie['name'] ?></p>
                <p class="card-text"><?= substr($movie['description'], 0, 100) . '...'; ?></p>
                <a href="/film/show.php/<?= $movie[0] ?>" class="btn btn-primary">Voir</a>
              </div>
            </div>
          <?php }
        }else{
          foreach ($moviesFav as $movie) { ?>
            <div class="card col-3">
                <img class="card-img-top" src="<?= $movie['image'] ?>" alt="Card image cap" style="width:190px;" />
                <div class="card-body">
                    <h2 class="card-title"><?= $movie['title'] ?></h2>
                    <p class="card-text"><?= substr($movie['description'], 0, 100) . '...'; ?></p>
                    <a href="/film/show.php/<?= $movie[0] ?>" class="btn btn-primary">Voir</a>
                </div>
            </div>
        <?php } 
        }
      }
      ?>

      </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
  </body>

  </html>

