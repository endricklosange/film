<?php
require_once 'db/connect.php';
require_once 'component/navbar.php';
require_once 'component/head.php';
require_once 'class/Users.php';

$pdo = new \PDO(DSN, USER, PASS);
session_start();
$urlID = str_replace('/', '', $_SERVER['PATH_INFO']);

$query = "SELECT film.id,`title`, categories.id,`image`,`description`,`image`,`name` FROM `film` INNER JOIN categories ON category = categories.id WHERE categories.id = $urlID;";
$statement = $pdo->query($query);
$movies = $statement->fetchAll();
if (isset($_POST['submitFav'])) {
   $moviesId = $_POST['prodId'];
   $CurrentUserID = $_SESSION['Log']->getId();
$queryAdd = "INSERT INTO `favorites`(`film_id`, `user_id`) VALUES ('$moviesId','$CurrentUserID')";
$pdo->exec($queryAdd);
}
?>

<html lang="fr">

<head>
    <?php
    head()
    ?>
    <title>Document</title>
</head>

<body>
    <?php
    navbar()
    ?>
    <div class="container">
        <h1><?= $movies[0]['name'] ?></h1>
        <div class="row">
            <?php
            foreach ($movies as $movie){ ?>
                <div class="card col-6">
                    <img class="card-img-top" src="<?= $movie['image'] ?>" alt="Card image cap" style="width:190px;" />
                    <div class="card-body">
                        <h2 class="card-title"><?= $movie['title'] ?></h2>
                        <p class="card-text"><?= substr($movie['description'], 0, 100) . '...'; ?></p>
                        <a href="/film/show.php/<?= $movie[0] ?>" class="btn btn-primary">Voir</a>
                        <form class="d-flex" method="POST">
                        <input id="prodId" name="prodId" type="hidden" value="<?= $movie[0] ?>">
                            <button class="btn btn-outline-success" name="submitFav"  type="submit">Favoris</button>
                        </form>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>

</html>